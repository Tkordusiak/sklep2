package sg.sklep;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class LogFilter implements javax.servlet.Filter{


    @Override
    public void init(FilterConfig filterConfig)
            throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException,
            ServletException {
            String ipAddress = request.getRemoteAddr();
        System.out.println("IP" + ipAddress + ", Time" + new Date().toString());

        if (warunekDoZablokowania()){
            ((HttpServletResponse)response).setStatus(404);
        }

        //chain.doFilter(request, response);
    }

    private boolean warunekDoZablokowania() {
        return true;
    }

    @Override
    public void destroy() {

    }
}
